/*
 * Project 113microPump
 * Description: PWM control of CCP1 motor
 * Author: Bjorn de Wagenaar
 * Date: 190130


 Experiment GraftPump
 */

 //SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 #include "math.h"

int md1_enable = D0;            /* TOP left motors, left motor driver */
int md2_enable = D1;            /* BOTTOM left motors, 2nd from the left motor driver */
int md3_enable = D2;            /* TOP right motors, 2nd fro mthe right motor driver */
int md4_enable = D3;            /* BOTTOM right motores, right motor driver */

int md1_phase = D4;
int md2_phase = D5;
int md3_phase = D6;
int md4_phase = D7;

bool dir_md1 = LOW;         // Initial direction of the motor driver (md)
bool dir_md2 = LOW;
bool dir_md3 = LOW;
bool dir_md4 = LOW;

bool bool_pump = false;
bool bool_pump2 = false;

// Pump setting 1
float wait_time = 30*60-4.72;     // Interval in seconds, equals 1 um/s, md2 and md4, Row D
float pump_time = 4.72;          // pump duration for pulsatile mode in seconds

// Pump setting 2
float wait_time2 = 30*60-14.26;    // Interval in seconds, equals 3 um/s, md1 and md3, Row P
float pump_time2 = 14.26;          // pump duration for pulsatile mode in seconds

unsigned long previousMillis;  //some global variables available anywhere in the program
unsigned long previousMillis2;  //some global variables available anywhere in the program
unsigned long currentMillis;

 void setup() {
   pinMode(md1_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md2_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md3_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md4_enable, OUTPUT);  /* Motor control pin enable */

   pinMode(md1_phase, OUTPUT);  /* Motor control pin phase,  */
   pinMode(md2_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md3_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md4_phase, OUTPUT);  /* Motor control pin phase */

   digitalWrite(md1_phase, dir_md1);  /* Write direction value to phase, chosing one direction */
   digitalWrite(md2_phase, dir_md2);
   digitalWrite(md3_phase, dir_md3);
   digitalWrite(md4_phase, dir_md4);

   previousMillis = millis();       //initial start time
   previousMillis2 = millis();
 }

 void loop() {

   currentMillis = millis();  //get the current "time" (actually the number of milli
   // Pump setting 1
   if ((currentMillis - previousMillis) > ((wait_time)*1000) && bool_pump == false){
     bool_pump = true;
     previousMillis = currentMillis;
   }

   if (bool_pump == false){
     analogWrite(md2_enable, 0); /* Write duty cycle 25% to output */
     analogWrite(md4_enable, 0);
   }

   if (bool_pump == true){
     analogWrite(md2_enable, 80); // Pump action condition 6
     analogWrite(md4_enable, 80); // Pump action condition 7
     if ((millis() - previousMillis) > (pump_time*1000)){
       bool_pump = false;
       previousMillis = millis();
     }
   }


   // Pump setting 2
   if ((currentMillis - previousMillis2) > ((wait_time2)*1000) && bool_pump2 == false){
     bool_pump2 = true;
     previousMillis2 = currentMillis;
   }

   if (bool_pump2 == false){
     analogWrite(md1_enable, 0); /* Write duty cycle 25% to output */
     analogWrite(md3_enable, 0);
   }

   if (bool_pump2 == true){
     analogWrite(md1_enable, 80); // Pump action condition 6
     analogWrite(md3_enable, 80); // Pump action condition 7
     if ((millis() - previousMillis2) > (pump_time2*1000)){
       bool_pump2 = false;
       previousMillis2 = millis();
     }
   }
 }
