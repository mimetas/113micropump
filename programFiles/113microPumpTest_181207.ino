/*
 * Project 113microPump
 * Description: PWM control of CCP1 motor
 * Author: Bjorn de Wagenaar
 * Date: 181004
 */

 SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 //SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 #include "math.h"

int md1_enable = D0;            /* Assigning pins */
int md2_enable = D1;
int md3_enable = D2;
int md4_enable = D3;

int md1_phase = D4;
int md2_phase = D5;
int md3_phase = D6;
int md4_phase = D7;

 void setup() {
   pinMode(md1_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md2_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md3_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md4_enable, OUTPUT);  /* Motor control pin enable */

   pinMode(md1_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md2_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md3_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md4_phase, OUTPUT);  /* Motor control pin phase */
 }

 void loop() {
   digitalWrite(md1_phase,LOW);  /* Write low to phase, chosing one direction */
   digitalWrite(md2_phase,LOW);
   digitalWrite(md3_phase,LOW);
   digitalWrite(md4_phase,LOW);

   analogWrite(md1_enable, 56);
   analogWrite(md2_enable, 48);
   analogWrite(md3_enable, 40);
   analogWrite(md4_enable, 32);
 }
