/*
 * Project 113microPump
 * Description: PWM control of CCP1 motor
 * Author: Bjorn de Wagenaar
 * Date: 181004


Experiment 181121 Plate 113-002
Experiment by Arnaud, two rows of chips with duty cycles 15, 25, 50 and 75 corresponding
to shear rates of 0.5, 1, 2 and 4 dyne cm2.

The plate is integrated with thermocouples to measure the temperature of the plate.
After 2 hours, the plate reached a temperature of 39 degrees Celsius. The incubator
was adjusted to 35 degrees. At this temperature, the cell culture showed a temperature
of approximately 37 degrees.

Pump 1 total runtime for all motors: approximately 24h. 
 */

 //SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 #include "math.h"

int md1_enable = D0;            /* Assigning pins */
int md2_enable = D1;
int md3_enable = D2;
int md4_enable = D3;

int md1_phase = D4;
int md2_phase = D5;
int md3_phase = D6;
int md4_phase = D7;

int factor = 255/100;

 void setup() {
   pinMode(md1_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md2_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md3_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md4_enable, OUTPUT);  /* Motor control pin enable */

   pinMode(md1_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md2_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md3_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md4_phase, OUTPUT);  /* Motor control pin phase */
 }

 void loop() {
   digitalWrite(md1_phase,LOW);  /* Write low to phase, chosing one direction */
   digitalWrite(md2_phase,LOW);
   digitalWrite(md3_phase,LOW);
   digitalWrite(md4_phase,LOW);

   analogWrite(md1_enable, round(15*factor)); /* Write duty cycle 25% to output */
   analogWrite(md2_enable, round(25*factor));
   analogWrite(md3_enable, round(50*factor));
   analogWrite(md4_enable, round(75*factor));
 }
