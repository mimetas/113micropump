/*
 * Project 113microPump
 * Description: PWM control of CCP1 motor
 * Author: Bjorn de Wagenaar
 * Date: 181126


 Experiment Pump2
 * Condition 2 - MD1 - Unidirectional 0.5 dyne cm2 duty cycle 15
 * Condition 3 - MD3 - Unidirectional 0.5 dyne cm2 duty cycle 15
 * Condition 4 - MD2 - Bidirectional 0.5 dyne cm2 duty cycle 15
 * Condition 5 - MD4 - Bidirectional 0.5 dyne cm2 duty cycle 15
 */

 //SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 #include "math.h"

int md1_enable = D0;            /* TOP left motors, left motor driver */
int md2_enable = D1;            /* BOTTOM left motors, 2nd from the left motor driver */
int md3_enable = D2;            /* TOP right motors, 2nd fro mthe right motor driver */
int md4_enable = D3;            /* BOTTOM right motores, right motor driver */

int md1_phase = D4;
int md2_phase = D5;
int md3_phase = D6;
int md4_phase = D7;

bool dir_md1 = LOW;         // Initial direction of the motor driver (md)
bool dir_md2 = LOW;
bool dir_md3 = LOW;
bool dir_md4 = LOW;

bool bool_pulsatile = false;

int factor = 255/100;       // Since the PWM can be set between 0-255, divide by 100 to be able to set the persentage
int interval = 1;           // Interval in minutes
int puls_time = 10;          // pump duration for pulsatile mode in seconds

unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;

 void setup() {
   pinMode(md1_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md2_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md3_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md4_enable, OUTPUT);  /* Motor control pin enable */

   pinMode(md1_phase, OUTPUT);  /* Motor control pin phase,  */
   pinMode(md2_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md3_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md4_phase, OUTPUT);  /* Motor control pin phase */

   digitalWrite(md1_phase, dir_md1);  /* Write direction value to phase, chosing one direction */
   digitalWrite(md2_phase, dir_md2);
   digitalWrite(md3_phase, dir_md3);
   digitalWrite(md4_phase, dir_md4);

   startMillis = millis();       //initial start time
 }

 void loop(){

   currentMillis = millis();  //get the current "time" (actually the number of milli

   if (currentMillis - startMillis > interval*60*1000){
     dir_md2 = !dir_md2;
     dir_md4 = !dir_md4;
     digitalWrite(md2_phase,dir_md2);
     digitalWrite(md4_phase,dir_md4);
     startMillis = millis();
   }

   analogWrite(md1_enable, round(20*factor)); // Pump action condition 2
   analogWrite(md2_enable, round(20*factor)); // Pump action condition 4
   analogWrite(md3_enable, round(20*factor)); // Pump action condition 3
   analogWrite(md4_enable, round(20*factor)); // Pump action condition 5

  }
