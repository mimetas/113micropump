/*
 * Project 113microPump
 * Description: PWM control of CCP1 motor
 * Author: Bjorn de Wagenaar
 * Date: 181004

 Experiment 181123 Plate 113-002
 Experiment by Arnaud, the pumps is switched upside down, using the same plate 113-002.
 1 row is seeded to connect to motor drivers 2 and 4 (bottom row of chips). The flow rate
 is set to a duty cycle of 15 and 25, corresponding to 0.5 and 1 dyne cm2 continuously.

 Thermocouples are integrated in the plate to monitor the temperature and to ajust the
 temperature of the incubator accordingly
  */

 //SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 #include "math.h"

 int md1_enable = D0;            /* TOP left motors, left motor driver */
 int md2_enable = D1;            /* BOTTOM left motors, 2nd from the left motor driver */
 int md3_enable = D2;            /* TOP right motors, 2nd fro mthe right motor driver */
 int md4_enable = D3;            /* BOTTOM right motors, right motor driver */

 int md1_phase = D4;
 int md2_phase = D5;
 int md3_phase = D6;
 int md4_phase = D7;

 int factor = 255/100;

 void setup(){
   pinMode(md1_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md2_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md3_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md4_enable, OUTPUT);  /* Motor control pin enable */

   pinMode(md1_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md2_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md3_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md4_phase, OUTPUT);  /* Motor control pin phase */
 }

 void loop() {
   digitalWrite(md1_phase,LOW);  /* Write low to phase, chosing one direction */
   digitalWrite(md2_phase,LOW);
   digitalWrite(md3_phase,LOW);
   digitalWrite(md4_phase,LOW);

   analogWrite(md1_enable, round(0)); /* Write duty cycle 25% to output */
   analogWrite(md2_enable, round(25*factor));
   analogWrite(md3_enable, round(0));
   analogWrite(md4_enable, round(15*factor));
 }
