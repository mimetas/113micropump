/*
 * Project 113microPump
 * Description: PWM control of CCP1 motor
 * Author: Bjorn de Wagenaar
 * Date: 181210


 Experiment Pump1
 * Condition 6 - MD1 - Unidirectional Pulsatile 10s on 100% duty cycle
 * Condition 7 - MD3 - Unidirectional Pulsatile
 * Condition 8 - MD2 - Bidirectional Pulsatile
 * Condition 9 - MD4 - Bidirectional Pulsatile
 */

 //SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 #include "math.h"

int md1_enable = D0;            /* TOP left motors, left motor driver */
int md2_enable = D1;            /* BOTTOM left motors, 2nd from the left motor driver */
int md3_enable = D2;            /* TOP right motors, 2nd fro mthe right motor driver */
int md4_enable = D3;            /* BOTTOM right motores, right motor driver */

int md1_phase = D4;
int md2_phase = D5;
int md3_phase = D6;
int md4_phase = D7;

bool dir_md1 = LOW;         // Initial direction of the motor driver (md)
bool dir_md2 = LOW;
bool dir_md3 = LOW;
bool dir_md4 = LOW;

bool bool_pulsatile = false;

int factor = 255/100;       // Since the PWM can be set between 0-255, divide by 100 to be able to set the persentage
int interval = 8;           // Interval in minutes
int puls_time = 10;          // pump duration for pulsatile mode in seconds

unsigned long startMillis;  //some global variables available anywhere in the program
unsigned long currentMillis;

 void setup() {
   pinMode(md1_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md2_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md3_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md4_enable, OUTPUT);  /* Motor control pin enable */

   pinMode(md1_phase, OUTPUT);  /* Motor control pin phase,  */
   pinMode(md2_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md3_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md4_phase, OUTPUT);  /* Motor control pin phase */

   digitalWrite(md1_phase, dir_md1);  /* Write direction value to phase, chosing one direction */
   digitalWrite(md2_phase, dir_md2);
   digitalWrite(md3_phase, dir_md3);
   digitalWrite(md4_phase, dir_md4);

   startMillis = millis();       //initial start time
 }

 void loop() {

   currentMillis = millis();  //get the current "time" (actually the number of milli

   if (currentMillis - startMillis > interval*60*1000){
     dir_md2 = !dir_md2;
     dir_md4 = !dir_md4;
     digitalWrite(md2_phase,dir_md2);
     digitalWrite(md4_phase,dir_md4);
     bool_pulsatile = true;
     startMillis = millis();
   }

   if (bool_pulsatile == false){
     analogWrite(md1_enable, round(0*factor)); /* Write duty cycle 25% to output */
     analogWrite(md2_enable, round(0*factor));
     analogWrite(md3_enable, round(0*factor));
     analogWrite(md4_enable, round(0*factor));
   }

   else if (bool_pulsatile == true){
     analogWrite(md1_enable, round(100*factor)); // Pump action condition 6
     analogWrite(md2_enable, round(100*factor)); // Pump action condition 8
     analogWrite(md3_enable, round(100*factor)); // Pump action condition 7
     analogWrite(md4_enable, round(100*factor)); // Pump action condition 9

     if (millis()-startMillis > puls_time*1000){
       bool_pulsatile = false;
     }
   }
 }
