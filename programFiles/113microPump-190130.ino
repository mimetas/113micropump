/*
 * Project 113microPump
 * Description: PWM control of CCP1 motor
 * Author: Bjorn de Wagenaar
 * Date: 190130
 * Plate: 113-009/010/011

 */

 //SYSTEM_MODE(AUTOMATIC);  // Photon particle automaticcally connects to the cloud
 SYSTEM_MODE(SEMI_AUTOMATIC); // Offline mode

 #include "math.h"

int md1_enable = D0;            /* TOP left motors, left motor driver */
int md2_enable = D1;            /* BOTTOM left motors, 2nd from the left motor driver */
int md3_enable = D2;            /* TOP right motors, 2nd fro mthe right motor driver */
int md4_enable = D3;            /* BOTTOM right motores, right motor driver */

int md1_phase = D4;
int md2_phase = D5;
int md3_phase = D6;
int md4_phase = D7;

bool dir_md1 = LOW;         // Initial direction of the motor driver (md)
bool dir_md2 = LOW;
bool dir_md3 = LOW;
bool dir_md4 = LOW;

bool bool_puls1 = false;
bool bool_puls2 = false;


int wait_time1 = 464;           // Interval in seconds. 25 µL of pumping every 8 min
int pump_time1 = 16;
int wait_time2 = 54.5;          // Interval in seconds. 3.125 µL of pumping every 1 min
int pump_time2 = 5.5;

unsigned long previousMillis1;
unsigned long previousMillis2;
unsigned long currentMillis;

 void setup() {
   pinMode(md1_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md2_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md3_enable, OUTPUT);  /* Motor control pin enable */
   pinMode(md4_enable, OUTPUT);  /* Motor control pin enable */

   pinMode(md1_phase, OUTPUT);  /* Motor  control pin phase,  */
   pinMode(md2_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md3_phase, OUTPUT);  /* Motor control pin phase */
   pinMode(md4_phase, OUTPUT);  /* Motor control pin phase */

   digitalWrite(md1_phase, dir_md1);  /* Write direction value to phase, chosing one direction */
   digitalWrite(md2_phase, dir_md2);
   digitalWrite(md3_phase, dir_md3);
   digitalWrite(md4_phase, dir_md4);

   previousMillis1 = millis();       //initial start time
   previousMillis2 = millis();       //initial start time
 }

 void loop() {

   currentMillis = millis();  //get the current "time" (actually the number of milli


   // Pump interval 1
   if ((currentMillis - previousMillis1) > ((wait_time1)*1000) && bool_puls1 == false){
     dir_md4 = !dir_md4;
     digitalWrite(md4_phase,dir_md4);
     bool_puls1 = true;
     previousMillis1 = currentMillis;
   }

   if (bool_puls1 == false){
     analogWrite(md3_enable, 0); /* Write duty cycle 25% to output */
     analogWrite(md4_enable, 0);
   }

   if (bool_puls1 == true){
     analogWrite(md3_enable, 255);
     analogWrite(md4_enable, 255);
     if ((millis() - previousMillis1) > (pump_time1*1000)){
       bool_puls1 = false;
       previousMillis1 = millis();
     }
   }

     // Pump interval 2
     if ((currentMillis - previousMillis2) > ((wait_time2)*1000) && bool_puls2 == false){
       dir_md2 = !dir_md2;
       digitalWrite(md2_phase,dir_md2);
       bool_puls2 = true;
       previousMillis2 = currentMillis;
     }

     if (bool_puls2 == false){
       analogWrite(md1_enable, 0); /* Write duty cycle 25% to output */
       analogWrite(md2_enable, 0);
     }

     if (bool_puls2 == true){
       analogWrite(md1_enable, 128);
       analogWrite(md2_enable, 128);
       if ((millis() - previousMillis2) > (pump_time2*1000)){
         bool_puls2 = false;
         previousMillis2 = millis();
       }

   }
 }
